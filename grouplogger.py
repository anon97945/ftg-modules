#    Friendly Telegram (telegram userbot) module
#    module author: @anon97945


import logging

from telethon.errors import MessageIdInvalidError

from .. import loader, utils
from telethon.tl.types import Channel

logger = logging.getLogger(__name__)


def represents_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


@loader.tds
class GroupLoggerMod(loader.Module):
    """Log given chats/channel to given group/channel"""
    strings = {"name": "Log Groups/Channel",
               "start": "<b>[Grouplogger]</b> Activated.</b>",
               "stopped": "<b>[Grouplogger]</b> Deactivated.</b>",
               "turned_off": "<b>[Grouplogger]</b> Is now turned off.</b>",
               "no_int": "<b>Your input was no int.</b>",
               "error": "<b>Your command was wrong.</b>",
               "settings": ("<b>[Grouplogger - Settings]</b> Current settings are:"
                             "\n{}.")}

    def __init__(self):
        self._ratelimit = []

    async def client_ready(self, client, db):
        self._db = db
        self._client = client
        self._me = await client.get_me(True)

    async def glcmd(self, message):
        """Available commands:
           .gl rem <chatid>
             - Removes given chat from watcher.
           .gl <chatid> <logchannelid>
             - Logs given groupchat in given channel
           .gl clearall
            - Clears the db of the module"""
        gl = self._db.get(__name__, "gl", [])
        sets = self._db.get(__name__, "sets", {})
        args = utils.get_args_raw(message).lower()
        args = str(args).split()
        if args[0] is not None and represents_int(args[0]):
            chatid = args[0]
            chatid_str = str(chatid)
        elif args[0] == "rem":
            chatid = args[1]
            chatid_str = str(chatid)
        elif args[0] != "clearall" and args[0] != "rem":
            return await utils.answer(message, self.strings("error", message))
        elif not args:
            return await utils.answer(message, self.strings("error", message))
        elif args[0] != "clearall" and args[1] is None:
            return await utils.answer(message, self.strings("error", message))

        if args:
            if args[0] == "clearall":
                self._db.set(__name__, "gl", [])
                self._db.set(__name__, "sets", {})
                return await utils.answer(message, self.strings("turned_off", message))
            elif args[0] == "rem" and represents_int(args[1]) and chatid_str in gl:
                gl.remove(chatid_str)
                sets.pop(chatid_str)
                self._db.set(__name__, "gl", gl)
                self._db.set(__name__, "sets", sets)
                return await utils.answer(message, self.strings("stopped", message))
            elif args[0] == "rem":
                if represents_int(args[1]) or chatid_str not in gl:
                    return await utils.answer(message, self.strings("error", message))
        if not represents_int(chatid_str):
            return await utils.answer(message, self.strings("error", message))
        if chatid_str not in gl:
            if not represents_int(args[0]) or not represents_int(args[1]):
                return await utils.answer(message, self.strings("no_int", message))
            gl.append(chatid_str)
            sets.setdefault(chatid_str, {})
            sets[chatid_str].setdefault("logchannel", args[1])
            self._db.set(__name__, "gl", gl)
            self._db.set(__name__, "sets", sets)
            return await utils.answer(message, self.strings("start", message))
        if chatid_str in gl:
            if args[0] is not None and args[1] is not None and chatid_str in gl:
                if not represents_int(args[0]) or not represents_int(args[1]):
                    return await utils.answer(message, self.strings("no_int", message))
                else:
                    sets[chatid_str].update({"logchannel": args[1]})
            self._db.set(__name__, "sets", sets)
            return await utils.answer(message, self.strings("settings", message).format(str(sets[chatid_str])))

    async def watcher(self, message):
        gl = self._db.get(__name__, "gl", [])
        sets = self._db.get(__name__, "sets", {})
        chatid = message.chat_id
        chatid_str = str(chatid)
        if message.is_private or chatid_str not in gl:
            return
        entity = await message.client.get_entity(message.sender_id)
        logchanid = int(sets[chatid_str].get("logchannel"))
        loggingchat = chatid
        chatsender = await message.get_sender()
        if chatsender is None:
            return
        senderid = (await message.get_sender()).id
        chattitle = (await message.get_chat()).title
        if chatsender.username:
            name = chatsender.username
            name = "@" + name
        elif not isinstance(entity, Channel):
            if chatsender.last_name:
                name = chatsender.first_name + " " + chatsender.last_name
            else:
                name = chatsender.first_name
        else:
            name = chatsender.title
        link = "Chat: " + str(chattitle) + " | <code>" + str(loggingchat) + "</code>" + "\nUser: " + str(name) + \
               " ID: " + "<a href='tg://user?id=" + str(senderid) + "'>" + str(senderid) + "</a>"
        try:
            await message.forward_to(logchanid)
            await message.client.send_message(logchanid, link)
            return
        except Exception as e:
            if "FORWARDS_RESTRICTED" in str(e):
                logger.error(loggingchat)
                logger.error(logchanid)
                logger.error(message.id)
                msgs = await message.client.get_messages(loggingchat, ids=message.id)
                await message.client.send_message(logchanid, message=msgs)
                await message.client.send_message(logchanid, link)
        except MessageIdInvalidError:
            return
