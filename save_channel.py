#    Friendly Telegram (telegram userbot) module
#    module author: @anon97945

import asyncio
import logging
import io
import re
from .. import loader, utils

logger = logging.getLogger(__name__)
regex = re.compile("(https://)?(t\.me/|telegram\.me/|telegram\.dog/)(c/)?(\d+|[a-zA-Z_0-9]+)/(\d+)$")


async def get_ids(link, message):
    match = regex.match(link)
    if not match:
        return await utils.answer(message, self.strings("invalid_link", message))
    chat_id = match.group(4)
    msg_id = int(match.group(5))
    if chat_id.isnumeric():
        chat_id  = int(("-100" + chat_id))
    return chat_id, msg_id


@loader.tds
class SaveChannelMod(loader.Module):
    """Saves Channel Message to SavedMessages"""
    strings = {"name": "Save Channel Message",
               "done": "<b>Forward complete.</b>",
               "invalid_link": "<b>Invalid link.</b>"}


    @loader.unrestricted
    async def sccmd(self, message):
        """.sc <postlink> to forward message to SavedMessages"""
        args = utils.get_args_raw(message).lower()
        if not args:
            return
        channel_id, msg_id = await get_ids(args, message)
        msgs = await message.client.get_messages(channel_id, ids=msg_id)
        await message.client.send_message("me", message=msgs)
        return await utils.answer(message, self.strings("done", message))

    async def schcmd(self, message):
        """.sch <postlink> to forward message to current chat"""
        args = utils.get_args_raw(message).lower()
        current_chat = message.chat_id
        if not args:
            return
        channel_id, msg_id = await get_ids(args, message)
        msgs = await message.client.get_messages(channel_id, ids=msg_id)
        await message.client.send_message(current_chat, message=msgs)
        return await message.delete()
