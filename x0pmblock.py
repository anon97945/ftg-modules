#    Friendly Telegram (telegram userbot)
#    Copyright (C) 2018-2019 The Authors

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging

from telethon import types, functions
from telethon.errors import MessageIdInvalidError

from .. import loader, utils

logger = logging.getLogger(__name__)


@loader.tds
class x0PMBlockMod(loader.Module):
    """Block and Report First Message x0 PMs"""
    strings = {"name": "x0 PM Blocker",
               "triggered": ("Hey! I don't appreciate you barging into my PM and send me scam!"
                             "\n\nPS: you've been reported as spam already.")}

    def __init__(self):
        self._ratelimit = []

    async def watcher(self, message):
        trigger1 = ".dlmod"
        trigger2 = "https://x0.at/"
        trigger3 = ".py"
        messagescount = 0
        if not message.is_private or not isinstance(message, types.Message):
            return
        chat = await message.get_chat()
        if chat.bot or chat.id == (await message.client.get_me(True)).user_id:
            return
        async for messages in message.client.iter_messages(entity=chat.id, limit=50, from_user=chat.id):
            if messages:
                messagescount += 1
        if messagescount > 1:
            return
        if trigger1 in message.text and trigger2 in message.text and trigger3 in message.text:
            try:
                await utils.answer(message, self.strings("triggered", message))
                await message.client.send_read_acknowledge(message.chat_id)
                await message.client(functions.contacts.BlockRequest(message.from_id))
                await message.client(functions.messages.ReportSpamRequest(peer=message.from_id))
                return
            except MessageIdInvalidError:
                return
