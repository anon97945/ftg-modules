#    Friendly Telegram (telegram userbot) module
#    module author: @anon97945


import asyncio
import logging

from telethon import events
from .. import loader, utils

logger = logging.getLogger(__name__)


@loader.tds
class otpMod(loader.Module):
    """Telegram OTP Remote Reciever"""
    strings = {"name": "OTP Reciever",
               "timeouterror": "<b>TimeoutError:</b>\nNo login code for 120 seconds recieved.",
               "error": "<b>No Login code in the message found.</b>",
               "otp_cfg_doc": "OTP Reciever config",
               "waiting": "<b>Waiting for the login code...</b>",
               "not_pc": "<b>This is no private chat. Use <code>.otp group --force</code></b>",
               "not_group": "This command is for groups only.",
               "no_self": "<b>You can't use it on yourself.</b>"}

    def __init__(self):
        self.name = self.strings["name"]
        self.config = loader.ModuleConfig(lambda m: self.strings("otp_cfg_doc", m))
        self._me = None
        self._ratelimit = []

    async def client_ready(self, client, db):
        self._client = client
        self._me = await client.get_me(True)

    @loader.owner
    async def otpcmd(self, event):
        """Available commands:
           .otp
             - waiting for the OTP from TG service chat, use in private
           .otp group --force
             - waiting for the OTP from TG service chat, use in group"""

        user_msg = utils.get_args_raw(event)
        chatid = event.chat_id
        logincode = False
        tgacc = 777000
        if chatid == (await event.client.get_me(True)).user_id:
            msgs = await utils.answer(event, self.strings("no_self", event))
            return
        if user_msg != "" and user_msg != "group --force":
            return
        if not event.is_private and user_msg != "group --force":
            msgs = await utils.answer(event, self.strings("not_pc", event))
            return
        elif event.is_private and user_msg == "group --force":
            msgs = await utils.answer(event, self.strings("not_group", event))
            return
        async with event.client.conversation(tgacc) as conv:
            try:
                msgs = await utils.answer(event, self.strings("waiting", event))
                logincode = conv.wait_event(events.NewMessage(incoming=True, from_users=tgacc), timeout=120)
                logincode = await logincode
                logincodemsg = " ".join((await event.client.get_messages(tgacc, 1,
                                                                         search="Login code:"))[0].message)
                if logincodemsg is not None:
                    if "Login code:" in logincodemsg.lower():
                        logincode = True
                if logincode:
                    await event.client.send_read_acknowledge(tgacc, clear_mentions=True)
                    await event.client.delete_messages(chatid, msgs)
                    msgs = await event.client.send_message(chatid, logincodemsg)
                    return
                else:
                    await event.client.delete_messages(chatid, msgs)
                    msgs = await event.client.send_message(chatid, self.strings("error", event))
                    return
            except asyncio.TimeoutError:
                await event.client.delete_messages(chatid, msgs)
                msgs = await event.client.send_message(chatid, self.strings("timeouterror", event))
                return
