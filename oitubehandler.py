#    Friendly Telegram (telegram userbot) module
#    module author: @anon97945

import asyncio
import logging
import re
import telethon.tl.types

from telethon.tl.types import MessageEntityUrl
from telethon import functions, events
from .. import loader, utils

logger = logging.getLogger(__name__)


async def buttonhandler(bmsg, chatid, botid, loopcount, sleeptimer, revbool, caption, search1, search2, search3):
    bmsg = await bmsg.client.get_messages(chatid, ids=bmsg.id)
    if bmsg.via_bot_id == botid:
        for x in range(loopcount):
            if x == loopcount:
                return False
            else:
                bmsg = await bmsg.client.get_messages(chatid, ids=bmsg.id)
                await asyncio.sleep(sleeptimer)
                if revbool and bmsg.buttons is not None:
                    buttons = reversed(bmsg.buttons)
                else:
                    buttons = bmsg.buttons
                    # logger.error(bmsg.buttons)
                if caption in bmsg.message:
                    for row in buttons:
                        for button in row:
                            # logger.error(button.text + " / " + search1)
                            if search1 in button.text and search2 in button.text and search3 in button.text:
                                await asyncio.sleep(0.5)
                                await button.click()
                                return True


async def waitfortext(bmsg, chatid, botid, loopcount, sleeptimer, search1, search2, search3):
    bmsg = await bmsg.client.get_messages(chatid, ids=bmsg.id)
    if bmsg.via_bot_id == botid:
        for x in range(loopcount):
            if x == loopcount:
                return False
            else:
                bmsg = await bmsg.client.get_messages(chatid, ids=bmsg.id)
                await asyncio.sleep(sleeptimer)
                if search1 in bmsg.message and search2 in bmsg.message and search3 in bmsg.message:
                    return True


def represents_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def to_bool(value):
    if str(value).lower() in ("true"):
        return True
    if str(value).lower() in ("false"):
        return False


@loader.tds
class oitubebotHandlerMod(loader.Module):
    """Handles the @oitubebot automatically with commands"""
    strings = {"name": "oitubebot Handler",
               "turned_off": "<b>[oitubebotHandler]</b> The mode is turned off in all chats and DB cleared.</b>",
               "no_int": "<b>Your input was no int.</b>",
               "noarchive": "<b>Archive is not set.</b>",
               "error": "<b>Your command was wrong.</b>",
               "watchererror": "<b>An error occured.</b>",
               "linkerror": "<b>Your given link was wrong.</b>",
               "settings": ("<b>[oitubebotHandler - Settings]</b> Current settings are:"
                             "\n\nWatcher Chats: {}\n Settings: {}.")}

    def __init__(self):
        self._ratelimit = []

    async def client_ready(self, client, db):
        self._db = db
        self._client = client
        self._me = await client.get_me(True)
        self.id = (await client.get_me(True)).user_id

    async def otbcmd(self, message):
        """Available commands:
           .otb init
             - Initiates the DB.
           .otb w <ID>
             - Adds the given id as watcher destination.
           .otb archive <ID>
             - Adds the given id as archive destination.
           .otb vq <quality>
             - Toggles given Quality. Example: 720
           .otb aq <quality>
             - Toggles given Quality. Example: 320
           .otb vformat <extension>
            - Toggles given Quality. Example: MP4
           .otb aformat <extension>
            - Toggles given Quality. Example: MP3
           .otb v <reply/link>
             - DL the video into the current chat.
           .otb a <reply/link>
             - DL the audio into the current chat.
           .otb av <reply/link>
             - DL the video into the archive.
           .otb aa <reply/link>
             - DL the audio into the archive.
           .otb clearall
            - Clears the db of the module"""
        regex = r"^(?:https?:\/\/)?(?:www\.|m\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?"
        botid = 639679145
        sleeptimer = 2.5
        wait = 15
        qualwait = 15
        dlwait = 180
        ulwait = 180
        chat = await message.get_chat()
        chatid = message.chat_id
        chatid_str = str(chatid)
        chatlinkid = str(chatid)[4:]
        args = utils.get_args_raw(message).lower()
        args = str(args).split()
        chats = self._db.get(__name__, "watcher", [])
        conf = self._db.get(__name__, "conf", {})
        dlaformat = conf.get("aformat")
        dlvformat = conf.get("vformat")
        dlvqualitys = conf.get("vquality")
        dlaqualitys = conf.get("aquality")
        archivechatid = conf.get("archive_id")
        if conf.get("archive_id") != "" and conf.get("archive_id") is not None:
            archivechatid = int(conf.get("archive_id"))
        elif args[0] == "aa" or args[0] == "av":
            return await utils.answer(message, self.strings("noarchive", message))
        if archivechatid != "":
            archivechatlinkid = str(archivechatid)[4:]

        if message.is_reply:
            replymsg = await message.get_reply_message()
            for (ent, url) in replymsg.get_entities_text():
                if isinstance(ent, MessageEntityUrl):
                    match = re.search(regex, url)
                    if match:
                        ytid = match.group(1)
                        args.append(ytid)
                    else:
                        return await utils.answer(message, self.strings("linkerror", message))

        if args is None:
            return

        if args[0] == "clearall":
            self._db.set(__name__, "watcher", [])
            self._db.set(__name__, "conf", {})
            return await utils.answer(message, self.strings("turned_off", message))

        if args[0] == "init":
            self._db.set(__name__, "watcher", [])
            self._db.set(__name__, "conf", {})
            await asyncio.sleep(2)
            chats = self._db.get(__name__, "watcher", [])
            conf = self._db.get(__name__, "conf", {})
            conf.setdefault("vquality", ["1080", "720", "480", "320", "240"])
            conf.setdefault("aquality", ["320", "256", "128", "64"])
            conf.setdefault("vformat", "MP4")
            conf.setdefault("aformat", "MP3")
            conf.setdefault("archive_id", "")
            self._db.set(__name__, "watcher", chats)
            self._db.set(__name__, "conf", conf)
            return await utils.answer(message, self.strings("settings", message).format(str(chats), str(conf)))

        if args[0] == "av" or args[0] == "v" and args[1] is not None:
            if args[0] == "av":
                chatid = int(conf.get("archive_id"))
            else:
                chatid = chatid
            await utils.answer(message, "Video wird bearbeitet...")
            del args[0]
            args = (' '.join(str(x) for x in args))
            results = await message.client.inline_query(botid, args)
            bmsg = await results[0].click(chatid)
            if not (await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, False, "", "😬 download 😐", "", "")):
                return await utils.answer(message, "Error download button not found")
            if not (await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, True, "😌 please select, required format: 😬", "Video", "", "")):
                return await utils.answer(message, "Error format button not found")
            for dlvquality in dlvqualitys:
                await utils.answer(message, "Trying " + dlvquality + " Quality...")
                if await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, True,"🙈 please select your required quality / format 🦾", dlvquality, dlvformat, ""):
                    break
            await utils.answer(message, "Video download wird gestartet..")
            if not (await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, False, "Upload Type: Video", "✅ Get TG File", "", "")):
                return await utils.answer(message, "Error Start Button not found...")
            if not (await waitfortext(bmsg, chatid, botid, qualwait, sleeptimer, "📞 contacting YouTuBe to get 🕹 download details", "", "")):
                return await utils.answer(message, "Error contacting YouTube caption not found...")
            await utils.answer(message, "Video download wird gestartet...")
            if not (await waitfortext(bmsg, chatid, botid, dlwait, sleeptimer, "🔜 uploading to Telegram", "", "")):
                return await utils.answer(message, "Error uploading caption not found...")
            await utils.answer(message, "Video upload wird gestartet...")
            if not (await waitfortext(bmsg, chatid, botid, ulwait, sleeptimer, "🍿", "👀", "|")):
                return await utils.answer(message, "Error upload finish caption not found...")
            if chatid == archivechatid:
                return await utils.answer(message, "Video erfolgreich archiviert! ✅\n\n👇👇👇\n https://t.me/c/" + archivechatlinkid + "/" + str(bmsg.id))
            else:
                return await message.client.delete_messages(chatid, message)


        if args[0] == "aa" or args[0] == "a" and args[1] is not None:
            if args[0] == "aa":
                chatid = archivechatid
            else:
                chatid = chatid
            await utils.answer(message, "Audio wird bearbeitet...")
            del args[0]
            args = (' '.join(str(x) for x in args))
            results = await message.client.inline_query(botid, args)
            bmsg = await results[0].click(chatid)
            if not (await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, False, "", "😬 download 😐", "", "")):
                return await utils.answer(message, "Error download button not found")
            if not (await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, True, "😌 please select, required format: 😬", "Audio", "", "")):
                return await utils.answer(message, "Error format button not found")
            for dlaquality in dlaqualitys:
                await utils.answer(message, "Trying " + dlaquality + " Quality...")
                if await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, True,"🙈 please select your required quality / format 🦾", dlaquality, dlaformat, ""):
                    break
            await utils.answer(message, "Audio download wird gestartet..")
            if not (await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, False, "Upload Type: Audio", "✅ Get TG File", "", "")):
                return await utils.answer(message, "Error Start Button not found...")
            if not (await waitfortext(bmsg, chatid, botid, qualwait, sleeptimer, "📞 contacting YouTuBe to get 🕹 download details", "", "")):
                return await utils.answer(message, "Error contacting YouTube caption not found...")
            await utils.answer(message, "Audio download wird gestartet...")
            if not (await waitfortext(bmsg, chatid, botid, dlwait, sleeptimer, "🔜 uploading to Telegram", "", "")):
                return await utils.answer(message, "Error uploading caption not found...")
            await utils.answer(message, "Audio upload wird gestartet...")
            if not (await waitfortext(bmsg, chatid, botid, ulwait, sleeptimer, "🍿", "👀", "|")):
                return await utils.answer(message, "Error upload finish caption not found...")
            if chatid == archivechatid:
                return await utils.answer(message, "Audio erfolgreich archiviert! ✅\n\n👇👇👇\n https://t.me/c/" + archivechatlinkid + "/" + str(bmsg.id))
            else:
                return await message.client.delete_messages(chatid, message)

        if args[0] == "w" and args[1] is not None:
            if not represents_int(args[1]):
                return await utils.answer(message, self.strings("no_int", message))
            else:
                if args[1] not in chats:
                    chats.append(args[1])
                else:
                    chats.remove(args[1])
            return await utils.answer(message, self.strings("settings", message).format(str(chats), str(conf)))

        if args[0] == "archive" and args[1] is not None:
            if not represents_int(args[1]):
                return await utils.answer(message, self.strings("no_int", message))
            else:
                conf.update({"archive_id": args[1]})
        elif args[0] == "vq" and args[1] is not None:
            if not represents_int(args[1]):
                return await utils.answer(message, self.strings("no_int", message))
            else:
                if not args[1] in conf["vquality"]:
                    conf["vquality"].append(args[1])
                else:
                    conf["vquality"].remove(args[1])
        elif args[0] == "aq" and args[1] is not None:
            if not represents_int(args[1]):
                return await utils.answer(message, self.strings("no_int", message))
            else:
                if not args[1] in conf["aquality"]:
                    conf["aquality"].append(args[1])
                else:
                    conf["aquality"].remove(args[1])
        elif args[0] == "aformat" and args[1] is not None:
            conf.update({"aformat": args[1].upper()})
        elif args[0] == "vformat" and args[1] is not None:
            conf.update({"vformat": args[1].upper()})
        return await utils.answer(message, self.strings("settings", message).format(str(chats), str(conf)))

    async def watcher(self, message):
        regex = r"^(?:https?:\/\/)?(?:www\.|m\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?"
        botid = 639679145
        sleeptimer = 2.5
        wait = 15
        qualwait = 15
        dlwait = 180
        ulwait = 180
        chat = await message.get_chat()
        chatid = message.chat_id
        chatid_str = str(chatid)
        chats = self._db.get(__name__, "watcher", [])
        conf = self._db.get(__name__, "conf", {})

        if chatid_str not in chats or message.via_bot_id == botid:
            return
        if message.sender_id == self.id:
            return

        dlvformat = conf.get("vformat")
        dlvqualitys = conf.get("vquality")
        for (ent, url) in message.get_entities_text():
            if isinstance(ent, MessageEntityUrl):
                match = re.search(regex, url)
                if match:
                    ytid = match.group(1)
                results = await message.client.inline_query(botid, ytid)
                bmsg = await results[0].click(chatid)
                if not (await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, False, "", "😬 download 😐", "", "")):
                    return await utils.answer(message, self.strings("watchererror", message))
                if not (await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, True, "😌 please select, required format: 😬", "Video", "", "")):
                    return await utils.answer(message, self.strings("watchererror", message))
                for dlvquality in dlvqualitys:
                    if await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, True,"🙈 please select your required quality / format 🦾", dlvquality, dlvformat, ""):
                        break
                if not (await buttonhandler(bmsg, chatid, botid, wait, sleeptimer, False, "YouTuBe Duration", "✅ Start Download", "", "")):
                    return await utils.answer(message, self.strings("watchererror", message))
                if not (await waitfortext(bmsg, chatid, botid, qualwait, sleeptimer, "📞 contacting YouTuBe to get 🕹 download details", "", "")):
                    return await utils.answer(message, self.strings("watchererror", message))
                if not (await waitfortext(bmsg, chatid, botid, dlwait, sleeptimer, "🔜 uploading to Telegram", "", "")):
                    return await utils.answer(message, self.strings("watchererror", message))
                if not (await waitfortext(bmsg, chatid, botid, ulwait, sleeptimer, "🍿", "👀", "|")):
                    return await utils.answer(message, self.strings("watchererror", message))
                if chat.admin_rights.delete_messages:
                    return await message.client.delete_messages(chatid, message)
