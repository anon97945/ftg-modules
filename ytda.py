from telethon import events
from .. import loader, utils

import asyncio
import logging

logger = logging.getLogger(__name__)


def register(cb):
    cb(ytdlaMod())


class ytdlaMod(loader.Module):

    strings = {"name": "YTDL Assist"}

    def __init__(self):
        self.name = self.strings["name"]
        self._me = None
        self._ratelimit = []

    async def client_ready(self, client, db):
        self._db = db
        self._client = client
        self.me = await client.get_me()

    async def ytvcmd(self, event):
        """.ytv"""
        user_msg = """{}""".format(utils.get_args_raw(event))
        ErrorMsg = False
        if event.fwd_from:
            return
        if not event.reply_to_msg_id:
            self_mess = True
            if not user_msg:
                return
        elif event.reply_to_msg_id:
            reply_message = await event.get_reply_message()
            self_mess = False
            if not reply_message.text:
                return

        chat = -1001456558777
        botid = 1084359292
        chhatlinkid = 1456558777

        async with event.client.conversation(chat) as conv:
            try:
                response = conv.wait_event(events.NewMessage(incoming=True, from_users=botid), timeout=10)
                response2 = conv.wait_event(events.NewMessage(incoming=True, from_users=botid), timeout=10)
                if not self_mess:
                    ytdlmsg = await event.client.forward_messages(chat, reply_message)
                else:
                    ytdlmsg = await event.client.send_message(chat, user_msg)
                await event.edit("wird bearbeitet..")
                await ytdlmsg.reply("/dl@hochlad_bot")
                response = await response
                responsemsg = await response.client.get_messages(chat, ids=response.message.id)

                response2 = await response2
                responsemsg = await response.client.get_messages(chat, ids=response.message.id)
                if responsemsg is not None:
                    if "error" in responsemsg.text.lower():
                        ErrorMsg = True
                if not ErrorMsg:
                    await event.client.send_read_acknowledge(chat, clear_mentions=True)
                    await event.edit("wird bearbeitet...")
                    await event.client.send_read_acknowledge(chat, clear_mentions=True)
                    await asyncio.sleep(1)
                    responsemsg = await response2.client.get_messages(chat, ids=response2.message.id)
                w = 0
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "1080p60 (MP4)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "1080p60 (WEBM)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "1080p30 (MP4)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "1080p30 (WEBM)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "1080p (MP4)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "1080p (WEBM)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "720p60 (MP4)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "720p60 (WEBM)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "720p30 (MP4)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "720p30 (WEBM)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "720p (MP4)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "720p (WEBM)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "480p (MP4)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "480p (WEBM)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "360p (MP4)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "360p (WEBM)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "240p (MP4)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "240p (WEBM)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "1080p" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "720p" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "Source (MP4)" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "hls-832" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                for row in responsemsg.buttons:
                    for button in row:
                        if not w > 0:
                            if "Unknown" in button.text:
                                await asyncio.sleep(2)
                                await button.click()
                                w += 1
                                break
                    await asyncio.sleep(2)
                    responsemsg = await response2.client.get_messages(chat, ids=response2.message.id)
                    await event.edit(responsemsg.text)
                    if "trying to download" in responsemsg.text:
                        await event.edit("Video erfolgreich archiviert! ✅\n\n👇👇👇\n https://t.me/c/"
                                         + str(chhatlinkid) + "/" + str(response2.message.id))
                    else:
                        await event.edit("⚠️ <b>ERROR</b> ⚠️")
                    return
                else:
                    await event.client.send_read_acknowledge(chat, clear_mentions=True)
                    await event.edit("⚠️ <b>ERROR</b> ⚠️\n" + responsemsg.text)
                    return
            except asyncio.TimeoutError:
                responsemsg = await response.client.get_messages(chat, ids=response.message.id)
                await event.edit("⚠️ <b>ERROR</b> ⚠️\n" + responsemsg.text)
                await event.client.send_read_acknowledge(chat, clear_mentions=True)
                return

    async def ytacmd(self, event):
        """.yta"""
        user_msg = """{}""".format(utils.get_args_raw(event))
        ErrorMsg = False
        if event.fwd_from:
            return
        if not event.reply_to_msg_id:
            self_mess = True
            if not user_msg:
                return
        elif event.reply_to_msg_id:
            reply_message = await event.get_reply_message()
            self_mess = False
            if not reply_message.text:
                return

        chat = -1001456558777
        botid = 1084359292
        chhatlinkid = 1456558777

        async with event.client.conversation(chat) as conv:
            try:
                response = conv.wait_event(events.NewMessage(incoming=True, from_users=botid), timeout=10)
                response2 = conv.wait_event(events.NewMessage(incoming=True, from_users=botid), timeout=10)
                if not self_mess:
                    ytdlmsg = await event.client.forward_messages(chat, reply_message)
                else:
                    ytdlmsg = await event.client.send_message(chat, user_msg)
                await event.edit("wird bearbeitet..")
                await ytdlmsg.reply("/dl@hochlad_bot")
                response = await response
                responsemsg = await response.client.get_messages(chat, ids=response.message.id)

                response2 = await response2
                responsemsg = await response.client.get_messages(chat, ids=response.message.id)
                if responsemsg is not None:
                    if "error" in responsemsg.text.lower():
                        ErrorMsg = True
                if not ErrorMsg:
                    await event.client.send_read_acknowledge(chat, clear_mentions=True)
                    await event.edit("wird bearbeitet...")
                    await event.client.send_read_acknowledge(chat, clear_mentions=True)
                    await asyncio.sleep(1)
                    responsemsg = await response2.client.get_messages(chat, ids=response2.message.id)
                    w = 0
                    for row in responsemsg.buttons:
                        for button in row:
                            if not w > 0:
                                if "MP3 (320 kbps)" in button.text:
                                    await asyncio.sleep(1)
                                    await button.click()
                                    w += 1
                                    break
                    for row in responsemsg.buttons:
                        for button in row:
                            if not w > 0:
                                if "MP3 (128 kbps)" in button.text:
                                    await asyncio.sleep(1)
                                    await button.click()
                                    w += 1
                                    break
                    for row in responsemsg.buttons:
                        for button in row:
                            if not w > 0:
                                if "MP3 (64 kbps)" in button.text:
                                    await asyncio.sleep(1)
                                    await button.click()
                                    w += 1
                                    break
                    await asyncio.sleep(2)
                    responsemsg = await response2.client.get_messages(chat, ids=response2.message.id)
                    await event.edit(responsemsg.text)
                    if "trying to download" in responsemsg.text:
                        await event.edit("Audio erfolgreich archiviert! ✅\n\n👇👇👇\n https://t.me/c/"
                                         + str(chhatlinkid) + "/" + str(response2.message.id))
                    else:
                        await event.edit("⚠️ <b>ERROR</b> ⚠️")
                    return
                else:
                    await event.client.send_read_acknowledge(chat, clear_mentions=True)
                    await event.edit("⚠️ <b>ERROR</b> ⚠️\n" + responsemsg.text)
                    return
            except asyncio.TimeoutError:
                responsemsg = await response.client.get_messages(chat, ids=response.message.id)
                await event.edit("⚠️ <b>ERROR</b> ⚠️\n" + responsemsg.text)
                await event.client.send_read_acknowledge(chat, clear_mentions=True)
                return
