from telethon import events
from telethon.errors.rpcerrorlist import YouBlockedUserError
from .. import loader, utils


def register(cb):
    cb(SangmataMod())


class SangmataMod(loader.Module):
    """Simple SangMataInfo Module"""

    strings = {"name": "Sangmata"}

    def __init__(self):
        self.name = self.strings["name"]
        self._me = None
        self._ratelimit = []

    async def client_ready(self, client, db):
        self._db = db
        self._client = client
        self.me = await client.get_me()

    async def csmcmd(self, event):
        """Check SangMataInfo
           .csm <reply>
           .csm <userid>"""
        user_msg = """{}""".format(utils.get_args_raw(event))
        command = "/search_id "
        summary = ""
        reply_id = ""
        id = ""
        if event.fwd_from:
            return
        if not event.reply_to_msg_id:
            self_mess = True
            if not user_msg:
                return
            else:
                id = user_msg
                user_msg = user_msg
        elif event.reply_to_msg_id and user_msg:
            reply_id = (await event.get_reply_message()).sender_id
            self_mess = True
        elif event.reply_to_msg_id:
            reply_id = (await event.get_reply_message()).sender_id
            id = str(reply_id)
            self_mess = False
        chat = "@SangMata_bot"
        reply_message = str(reply_id)
        await event.edit("<code>Im waiting for a response from the Bot ...</code>")
        async with event.client.conversation(chat) as conv:
            try:
                response = conv.wait_event(events.NewMessage(incoming=True, from_users=300860929))
                response2 = conv.wait_event(events.NewMessage(incoming=True, from_users=300860929))
                response3 = conv.wait_event(events.NewMessage(incoming=True, from_users=300860929))
                if not self_mess:
                    await event.client.send_message(chat, reply_message)
                else:
                    await event.client.send_message(chat, user_msg)
                response = await response
                if not "No records" in response.text:
                    response2 = await response2
                else:
                    await event.client.send_read_acknowledge(response.chat_id)
                    await event.edit(response.text + " for " + id)
                    return
                if not "No records" in response2.text:
                    response3 = await response3
                else:
                    await event.client.send_read_acknowledge(response.chat_id)
                    await event.edit(response2.text + " for " + id)
                    return
                await event.client.send_read_acknowledge(response.chat_id)
                if "Username History" in response.text:
                    summary = summary + response.text + "\n\n"
                elif "Name History" in response.text:
                    summary = summary + response.text + "\n\n"
                if "Username History" in response2.text:
                    summary = summary + response2.text + "\n\n"
                elif "Name History" in response2.text:
                    summary = summary + response2.text + "\n\n"
                if "Username History" in response3.text:
                    summary = summary + response3.text + "\n\n"
                elif "Name History" in response3.text:
                    summary = summary + response3.text + "\n\n"
            except YouBlockedUserError:
                await event.reply("<code>Unblock </code> @SangMata_bot")
            await event.edit(summary)
