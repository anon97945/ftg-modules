#    Friendly Telegram (telegram userbot) module
#    module author: @anon97945


import asyncio
import logging

from telethon import functions
from datetime import timedelta
from telethon.tl.types import User, Channel, ChatBannedRights
from telethon.errors import UserNotParticipantError

from .. import loader, utils

logger = logging.getLogger(__name__)


async def is_linkedchannel(e, c, u, message):
    if not isinstance(e, User):
        full_chat = await message.client(functions.channels.GetFullChannelRequest(channel=u))
        if full_chat.full_chat.linked_chat_id:
            linked_channel_id = int(str(-100) + str(full_chat.full_chat.linked_chat_id))
            if c == linked_channel_id:
                return True
            else:
                return False
    else:
        return False


def represents_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def to_bool(value):
    if str(value).lower() in ("true"):
        return True
    if str(value).lower() in ("false"):
        return False


@loader.tds
class BlockChannelUserMod(loader.Module):
    """Block messages of channelusers"""
    strings = {"name": "Block Channel User",
               "not_dc": "<b>This is no Groupchat.</b>",
               "start": "<b>[BlockChannelUser]</b> Activated in this chat.</b>",
               "stopped": "<b>[BlockChannelUser]</b> Deactivated in this chat.</b>",
               "turned_off": "<b>[BlockChannelUser]</b> The mode is turned off in all chats.</b>",
               "no_int": "<b>Your input was no int.</b>",
               "error": "<b>Your command was wrong.</b>",
               "permerror": "<b>You have no delete permissions in this chat.</b>",
               "settings": ("<b>[BlockChannelUser - Settings]</b> Current settings in this"
                             " chat are:\n{}."),
               "triggered": ("{}, you can't write as a channel here.")}

    def __init__(self):
        self._ratelimit = []

    async def client_ready(self, client, db):
        self._db = db
        self._client = client
        self._me = await client.get_me(True)

    async def bcucmd(self, message):
        """Available commands:
           .bcu
             - Toggles the module for the current chat.
           .bcu notify <true/false>
             - Toggles the notification message.
           .bcu ban <minutes/or 0>
            - Bans the user for x minutes. 0 to disable.
           .bcu deltimer <seconds/or 0>
            - Deletes the notification message in seconds. 0 to disable.
           .bcu clearall
            - Clears the db of the module"""
        bcu = self._db.get(__name__, "bcu", [])
        sets = self._db.get(__name__, "sets", {})
        args = utils.get_args_raw(message).lower()
        args = str(args).split()
        chat = await message.get_chat()
        chatid = message.chat_id
        chatid_str = str(chatid)

        if args:
            if args[0] == "clearall":
                self._db.set(__name__, "bcu", [])
                self._db.set(__name__, "sets", {})
                return await utils.answer(message, self.strings("turned_off", message))

        if message.is_private:
            await utils.answer(message, self.strings("not_dc", message))
            return

        if not chat.admin_rights and not chat.creator:
            return await utils.answer(message, self.strings("permerror", message))
        else:
            if not chat.admin_rights.delete_messages:
                return await utils.answer(message, self.strings("permerror", message))

        if not args:
            if chatid_str not in bcu:
                bcu.append(chatid_str)
                sets.setdefault(chatid_str, {})
                sets[chatid_str].setdefault("notify", True)
                sets[chatid_str].setdefault("ban", 60)
                sets[chatid_str].setdefault("deltimer", 60)
                self._db.set(__name__, "bcu", bcu)
                self._db.set(__name__, "sets", sets)
                return await utils.answer(message, self.strings("start", message))
            else:
                bcu.remove(chatid_str)
                sets.pop(chatid_str)
                self._db.set(__name__, "bcu", bcu)
                self._db.set(__name__, "sets", sets)
                return await utils.answer(message, self.strings("stopped", message))

        if chatid_str in bcu:
            if args[0] == "notify" and args[1] is not None and chatid_str in bcu:
                if not isinstance(to_bool(args[1]), bool):
                    return await utils.answer(message, self.strings("error", message))
                sets[chatid_str].update({"notify": to_bool(args[1])})
            elif args[0] == "ban" and args[1] is not None and chatid_str in bcu:
                if not represents_int(args[1]):
                    return await utils.answer(message, self.strings("no_int", message))
                else:
                    sets[chatid_str].update({"ban": args[1].capitalize()})
            elif args[0] == "deltimer" and args[1] is not None and chatid_str in bcu:
                if not represents_int(args[1]):
                    return await utils.answer(message, self.strings("no_int", message))
                else:
                    sets[chatid_str].update({"deltimer": args[1]})
            elif args[0] != "settings" and chatid_str in bcu:
                return
            self._db.set(__name__, "sets", sets)
            return await utils.answer(message, self.strings("settings", message).format(str(sets[chatid_str])))

    async def watcher(self, message):
        bcu = self._db.get(__name__, "bcu", [])
        sets = self._db.get(__name__, "sets", {})
        chatid = message.chat_id
        chatid_str = str(chatid)
        if message.is_private or chatid_str not in bcu:
            return
        chat = await message.get_chat()
        user = await message.get_sender()
        userid = message.sender_id
        entity = await message.client.get_entity(message.sender_id)

        if (await is_linkedchannel(entity, chatid, userid, message)):
            return
        if not chat.admin_rights and not chat.creator:
            return
        else:
            if not chat.admin_rights.delete_messages:
                return
        if isinstance(entity, Channel):
            usertag = "<a href=tg://user?id=" + str(userid) + ">" + user.title + \
                      "</a> (<code>" + str(userid) + "</code>)"
            await message.delete()
            if chat.admin_rights.ban_users and sets[chatid_str].get("ban") is not None:
                if sets[chatid_str].get("ban") != "0":
                    BANTIMER = sets[chatid_str].get("ban")
                    await message.client(functions.channels.EditBannedRequest(chatid, userid,
                                         ChatBannedRights(until_date=timedelta(minutes=BANTIMER),
                                         view_messages=True, send_messages=False)))
            if sets[chatid_str].get("notify") is True:
                msgs = await utils.answer(message, self.strings("triggered", message).format(usertag))
                if sets[chatid_str].get("deltimer") != "0":
                    DELTIMER = int(sets[chatid_str].get("deltimer"))
                    await asyncio.sleep(DELTIMER)
                    await message.client.delete_messages(chatid, msgs)
