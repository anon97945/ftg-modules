from .. import loader, utils
from telethon.tl.types import ChatBannedRights as cb
from telethon.tl.functions.channels import EditBannedRequest as eb


@loader.tds
class AntiRaidMod(loader.Module):
    """AntiRaid-Modul."""
    strings = {"name": "AntiRaid"}

    async def client_ready(self, client, db):
        self.db = db

    async def antiraidcmd(self, message):
        """Enable / disable AntiRaid mode. Usage : .antiraid <clearall* (optional)>. \
           \n* - turns the mode in all chats off!"""
        ar = self.db.get("AntiRaid", "ar", [])
        sets = self.db.get("AntiRaid", "sets", {})
        args = utils.get_args_raw(message)

        if args == "clearall":
            self.db.set("AntiRaid", "ar", [])
            self.db.set("AntiRaid", "action", {})
            return await message.edit("<b>[AntiRaid]</b> The mode is turned off in all chats .")

        if not message.is_private:
            chat = await message.get_chat()
            if not chat.admin_rights and not chat.creator:
                return await message.edit("<b>I'm not the admin here.</b>")
            else:
                if not chat.admin_rights.ban_users:
                    return await message.edit("<b>I do not have the required rights.</b>")

            chatid = str(message.chat_id)
            if chatid not in ar:
                ar.append(chatid)
                sets.setdefault(chatid, {})
                sets[chatid].setdefault("stats", 0)
                sets[chatid].setdefault("action", "kick")
                self.db.set("AntiRaid", "ar", ar)
                self.db.set("AntiRaid", "sets", sets)
                return await message.edit("<b>[AntiRaid]</b> Activated in this chat.")

            else:
                ar.remove(chatid)
                sets.pop(chatid)
                self.db.set("AntiRaid", "ar", ar)
                self.db.set("AntiRaid", "sets", sets)
                return await message.edit("<b>[AntiRaid]</b> Deactivated in this chat.")

        else:
            return await message.edit("<b>[AntiRaid]</b> This is not a chat!")

    async def swatscmd(self, message):
        """AntiRaid module settings. Usage: .swats <kick/ban/mute/clear>; nothing."""
        if not message.is_private:
            ar = self.db.get("AntiRaid", "ar", [])
            sets = self.db.get("AntiRaid", "sets", {})
            chatid = str(message.chat_id)
            args = utils.get_args_raw(message)
            if chatid in ar:
                if args:
                    if args == "kick":
                        sets[chatid].update({"action": "kick"})
                    elif args == "ban":
                        sets[chatid].update({"action": "ban"})
                    elif args == "mute":
                        sets[chatid].update({"action": "mute"})
                    elif args == "clear":
                        sets[chatid].pop("stats")
                        self.db.set("AntiRaid", "sets", sets)
                        return await message.edit("<b>[AntiRaid - Settings]</b> Chat stats reseted.")
                    else:
                        return await message.edit("<b>[AntiRaid - Settings]</b> This mode is not on the list. \
                                                   \nAvailable modes: kick/ban/mute.")

                    self.db.set("AntiMention", "sets", sets)
                    return await message.edit(f"<b>[AntiRaid - Settings]</b> Now, when participants log in, \
                                              the following action will be performed: {sets[chatid]['action']}.")
                else:
                    return await message.edit("<b>[AntiRaid - Settings]</b> Chat settings:\n\n"
                                              "<b>Mode state:</b> True\n"
                                              f"<b>When participants enter, this action will be performed:</b> \
                                              {sets[chatid]['action']}\n"
                                              f"<b>Total users:</b> {sets[chatid]['stats']}")
            else:
                return await message.edit("<b>[AntiRaid - Settings]</b> In this chat the mode is deactivated.")
        else:
            return await message.edit("<b>[AntiRaid]</b> This is not a chat!")

    async def watcher(self, message):
        try:
            ar = self.db.get("AntiRaid", "ar", [])
            sets = self.db.get("AntiRaid", "sets", {})
            chatid = str(message.chat_id)
            if chatid not in ar:
                return
            if message.user_joined or message.user_added:
                user = await message.get_user()
                if sets[chatid]["action"] == "kick":
                    await message.client.kick_participant(int(chatid), user.id)
                elif sets[chatid]["action"] == "ban":
                    await message.client(eb(int(chatid), user.id, cb(until_date=None, view_messages=True)))
                elif sets[chatid]["action"] == "mute":
                    await message.client(eb(int(chatid), user.id, cb(until_date=True, send_messages=True)))
                sets[chatid].update({"stats": sets[chatid]["stats"] + 1})
                return self.db.set("AntiRaid", "sets", sets)
        except:
            return
