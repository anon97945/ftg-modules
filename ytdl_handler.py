#    Friendly Telegram (telegram userbot) module
#    module author: @anon97945

import asyncio
import logging
import datetime
import re

from telethon.tl.types import MessageEntityUrl, Message
from telethon.errors.rpcerrorlist import YouBlockedUserError
from telethon import events
from .. import loader, utils

logger = logging.getLogger(__name__)


async def buttonhandler(bmsg, chatid, otbot_id, loopcount, sleeptimer, revbool, caption, search1, search2, search3):
    bmsg = await bmsg.client.get_messages(chatid, ids=bmsg.id)
    if bmsg.via_bot_id == otbot_id:
        for x in range(loopcount):
            if x == loopcount:
                return False
            else:
                bmsg = await bmsg.client.get_messages(chatid, ids=bmsg.id)
                await asyncio.sleep(sleeptimer)
                if revbool and bmsg.buttons is not None:
                    buttons = reversed(bmsg.buttons)
                else:
                    buttons = bmsg.buttons
                    # logger.error(bmsg.buttons)
                if caption in bmsg.message:
                    for row in buttons:
                        for button in row:
                            # logger.error(button.text + " / " + search1)
                            if search1 in button.text and search2 in button.text and search3 in button.text:
                                await asyncio.sleep(0.2)
                                await button.click()
                                return True


async def waitfortext(bmsg, chatid, otbot_id, loopcount, sleeptimer, search1, search2, search3):
    bmsg = await bmsg.client.get_messages(chatid, ids=bmsg.id)
    if bmsg.via_bot_id == otbot_id:
        for x in range(loopcount):
            if x == loopcount:
                return False
            else:
                bmsg = await bmsg.client.get_messages(chatid, ids=bmsg.id)
                await asyncio.sleep(sleeptimer)
                if search1 in bmsg.message and search2 in bmsg.message and search3 in bmsg.message:
                    return True


def to_bool(value):
    if str(value).lower() in ("true"):
        return True
    if str(value).lower() in ("false"):
        return False


def represents_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


async def ytdlbot_handler(vid_link, chatid, event, timer, ytdlbot_id):
    try:
        async with event.conversation(chatid) as conv:
            try:
                response = conv.wait_event(events.NewMessage(incoming=True, from_users=ytdlbot_id), timeout=timer)
                await event.send_message(ytdlbot_id, vid_link)
                response = await response
                if "not made this video available in your country" in response.text:
                    await event.send_read_acknowledge(response.chat_id, response.message)
                    errorstring = response.text
                    return False, errorstring
                if "ERROR" in response.text:
                    await event.send_read_acknowledge(response.chat_id, response.message)
                    errorstring = response.text
                    return False, errorstring
                await event.send_message(chatid, response.message)
                await event.send_read_acknowledge(response.chat_id, response.message)
                errorstring = ""
                return True, errorstring
            except YouBlockedUserError:
                errorstring = "unblock_bot"
                return False, errorstring
            except asyncio.TimeoutError:
                errorstring = "timeout_error"
                return False, errorstring
    except Exception as e:
        if "already has one open conversation" in str(e):
            errorstring = "already_handling"
            return False, errorstring
        else:
            errorstring = str(e)
            return False, errorstring


class Queue:
    def __init__(self):
        self.queue = []

    def front_element(self):
        return self.queue[-1]

    def rear_element(self):
        return self.queue[0]

    def isEmpty(self):
        if len(self.queue) <= 0:
            return True
        else:
            return False

    def clQueue(self):
        return self.queue.clear()

    def enQueue(self, val):
        self.queue.append(val)

    def cuQueue(self):
        if self.isEmpty():
            return "Queue is empty."
        else:
            return self.queue[0]

    def coQueue(self):
        return len(self.queue)

    def deQueue(self):
        if self.isEmpty():
            return "Queue is empty."
        else:
            return self.queue.pop(0)


@loader.tds
class ytdlHandlerMod(loader.Module):
    """Handles the @YtbDownBot automatically with commands"""
    strings = {"name": "youtube-dl Handler",
               "turned_off": "<b>[YTDL Handler]</b> The mode is turned off in all chats and DB cleared.</b>",
               "no_int": "<b>Your input was no int.</b>",
               "noarchive": "<b>Archive is not set.</b>",
               "error": "<b>Your command was wrong.</b>",
               "unblock_bot": "<code>Unblock </code> @YtbDownBot",
               "no_video": "<code>Bot responded with non-media format, try again.</code>",
               "chat_link": "Video successfully archived! ✅\n\n👇👇👇\n https://t.me/c/{}/{}",
               "timeout_error": "Error: The download of <code>{}</code> took too long.",
               "already_handling": "There is already a video in the queue, please try again later!",
               "unknown_error": "Error: There was an undefined error.\n\n<code>{}</code>",
               "waiting": "Video is being edited. <b>A maximum of 1 video is possible at the same time!</b> ",
               "max_videos": "Too many links in 5 minutes. Please try again later.",
               "queue_start": "Processing <code>{}</code>.",
               "watchererror": "<b>An error occured.</b>",
               "queued": "<code>{}</code> was placed in the queue at position {}. The download can take a while.",
               "settings": ("<b>[YTDL Handler - Settings]</b> Current settings are:"
                             "\n\nWatcher Chats: {}\n Settings: {}.")}

    def __init__(self):
        self._ratelimit = []
        self.q = Queue()
        self.qw_enabled = False
        self._task = asyncio.create_task(self._start_queue())

    async def client_ready(self, client, db):
        self._db = db
        self._client = client
        self._me = await client.get_me(True)
        self.id = (await client.get_me(True)).user_id
        self.splitter = "|"
        self.ytdlbot_id = 794633388
        self.otbot_id = 639679145
        self.otbsleeptimer = 2.5
        self.otbwait = 15
        self.otbqualwait = 15
        self.otbdlwait = 180
        self.otbulwait = 180
        self.ytregex = r"^(?:https?:\/\/)?(?:www\.|m\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?"
        self.otbdlvqualitys = ["1080", "720", "480", "320", "240"]
        self.otbdlvformat = "MP4"

    async def _start_queue(self):
        ytdlbot_id = self.ytdlbot_id
        otbot_id = self.otbot_id
        otbsleeptimer = self.otbsleeptimer
        otbwait = self.otbwait
        otbqualwait = self.otbqualwait
        otbdlwait = self.otbdlwait
        otbulwait = self.otbulwait
        otbdlvqualitys = self.otbdlvqualitys
        otbdlvformat = self.otbdlvformat
        self_id = self.id
        splitter = self.splitter
        if self.qw_enabled is False:
            self.qw_enabled = True
        else:
            self.qw_enabled = False
        while self.qw_enabled is True:
            if not self.q.isEmpty():
                conf = self._db.get(__name__, "conf", {})
                if conf.get("timeout") == "" or conf.get("timeout") is None:
                    timer = 900
                else:
                    timer = int(conf.get("timeout"))
                cooldown = datetime.datetime.now() + datetime.timedelta(seconds=75)
                (vid_link, chatid_str, msgid_str, replymsgid_str,
                    ytlinkstr, archivestr) = str(self.q.cuQueue()).split(splitter)
                chatid = int(chatid_str)
                chat = await self._client.get_entity(chatid)
                UseOitubebot = False
                queuemsg = await self._client.send_message(chatid, self.strings(
                    "queue_start", chatid).format(vid_link))
                if not to_bool(ytlinkstr):
                    # try @YtbDownBot
                    await self._client.delete_messages(chatid, replymsgid_str)
                    responded, errorstring = await ytdlbot_handler(vid_link, chatid, self._client, timer, ytdlbot_id)
                    await self._client.delete_messages(chatid, queuemsg)
                    if responded and errorstring == "":
                        await self._client.delete_messages(chatid, replymsgid_str)
                        if chat.admin_rights.delete_messages:
                            await self._client.delete_messages(chatid, msgid_str)
                    elif not responded and "already_handling" in errorstring:
                        await self._client.send_message(chatid, self.strings("already_handling", chatid))
                        await self._client.delete_messages(chatid, replymsgid_str)
                    elif not responded and "timeout_error" in errorstring:
                        await self._client.send_message(chatid, self.strings("timeout_error", chatid).format(vid_link))
                        await self._client.delete_messages(chatid, replymsgid_str)
                        if chat.admin_rights.delete_messages:
                            await self._client.delete_messages(chatid, msgid_str)
                    elif not responded and "unblock_bot" in errorstring:
                        await self._client.send_message(chatid, self.strings("unblock_bot", chatid))
                        await self._client.delete_messages(chatid, replymsgid_str)
                        if chat.admin_rights.delete_messages:
                            await self._client.delete_messages(chatid, msgid_str)
                    elif not responded and "not made this video available in your country" in errorstring and UseOitubebot == True:
                        # try oitubebot
                        oitubeerror = False
                        match = re.search(self.ytregex, vid_link)
                        if match:
                            ytid = match.group(1)
                            vid_link = ytid
                        results = await self._client.inline_query(otbot_id, vid_link)
                        bmsg = await results[0].click(self_id)
                        if (await waitfortext(bmsg, self_id, otbot_id, otbqualwait, otbsleeptimer,
                                                  "ERROR", "", "")):
                            oitubeerror = True
                            results = await self._client.get_messages(self_id, ids=bmsg.id)
                            await self._client.delete_messages(self_id, bmsg.id)
                            await self._client.send_message(chatid, "<code>" + results.message + "</code>")
                            if chat.admin_rights.delete_messages:
                                await self._client.delete_messages(chatid, msgid_str)
                        if not oitubeerror:
                            if not (await buttonhandler(bmsg, self_id, otbot_id, otbwait, otbsleeptimer, False,
                                                        "", "😬 download 😐", "", "")):
                                await self._client.send_message(chatid, self.strings("watchererror", chatid))
                                if chat.admin_rights.delete_messages:
                                    await self._client.delete_messages(chatid, msgid_str)
                            if not (await buttonhandler(bmsg, self_id, otbot_id, otbwait, otbsleeptimer, True,
                                                        "😌 please select, required format: 😬", "Video", "", "")):
                                await self._client.send_message(chatid, self.strings("watchererror", chatid))
                                if chat.admin_rights.delete_messages:
                                    await self._client.delete_messages(chatid, msgid_str)
                            for otbdlvquality in otbdlvqualitys:
                                if await buttonhandler(bmsg, self_id, otbot_id, otbwait, otbsleeptimer, True,
                                                       "🙈 please select your required quality / format 🦾",
                                                       otbdlvquality, otbdlvformat, ""):
                                    break
                            if not (await buttonhandler(bmsg, self_id, otbot_id, otbwait, otbsleeptimer, False,
                                                        "Upload Type: Video", "✅ Get TG File", "", "")):
                                await self._client.send_message(chatid, self.strings("watchererror", chatid))
                                if chat.admin_rights.delete_messages:
                                    await self._client.delete_messages(chatid, msgid_str)
                            if not (await waitfortext(bmsg, self_id, otbot_id, otbqualwait, otbsleeptimer,
                                                      "📞 contacting YouTuBe to get 🕹 download details", "", "")):
                                await self._client.send_message(chatid, self.strings("watchererror", chatid))
                                if chat.admin_rights.delete_messages:
                                    await self._client.delete_messages(chatid, msgid_str)
                            if not (await waitfortext(bmsg, self_id, otbot_id, otbdlwait, otbsleeptimer,
                                                      "🔜 uploading to Telegram", "", "")):
                                await self._client.send_message(chatid, self.strings("watchererror", chatid))
                                if chat.admin_rights.delete_messages:
                                    await self._client.delete_messages(chatid, msgid_str)
                            if not (await waitfortext(bmsg, self_id, otbot_id, otbulwait, otbsleeptimer, "🍿", "👀", "|")):
                                await self._client.send_message(chatid, self.strings("watchererror", chatid))
                                if chat.admin_rights.delete_messages:
                                    await self._client.delete_messages(chatid, msgid_str)
                            results = await self._client.get_messages(self_id, ids=bmsg.id)
                            await self._client.delete_messages(self_id, bmsg.id)
                            await self._client.send_message(chatid, results)
                            if chat.admin_rights.delete_messages:
                                await self._client.delete_messages(chatid, msgid_str)
                    elif not responded and "not made this video available in your country" in errorstring:
                        await self._client.delete_messages(chatid, replymsgid_str)
                        if chat.admin_rights.delete_messages:
                            await self._client.delete_messages(chatid, msgid_str)
                    elif not responded:
                        await self._client.send_message(chatid, self.strings(
                            "unknown_error", chatid).format(errorstring))
                        await self._client.delete_messages(chatid, replymsgid_str)
                        if chat.admin_rights.delete_messages:
                            await self._client.delete_messages(chatid, msgid_str)
                    ctime = datetime.datetime.now()
                    if not ctime > cooldown and not cooldown < datetime.datetime.now():
                        remaining = cooldown - ctime
                        await asyncio.sleep(remaining.seconds + 2)
                elif to_bool(ytlinkstr) and UseOitubebot == True:
                    #oitubebot
                    oitubeerror = False
                    results = await self._client.inline_query(otbot_id, vid_link)
                    bmsg = await results[0].click(self_id)
                    await self._client.delete_messages(chatid, replymsgid_str)
                    if not (await buttonhandler(bmsg, self_id, otbot_id, otbwait, otbsleeptimer, False,
                                                "", "😬 download 😐", "", "")):
                        if not (await buttonhandler(bmsg, self_id, otbot_id, otbwait, otbsleeptimer, False,
                                                "", "ERROR", "", "")):
                            oitubeerror = True
                            await self._client.send_message(chatid, self.strings("watchererror", chatid))
                            await self._client.delete_messages(chatid, replymsgid_str)
                            await self._client.delete_messages(chatid, queuemsg)
                        await self._client.send_message(chatid, self.strings("watchererror", chatid))
                        await self._client.delete_messages(chatid, replymsgid_str)
                        await self._client.delete_messages(chatid, queuemsg)
                        if chat.admin_rights.delete_messages:
                            await self._client.delete_messages(chatid, msgid_str)
                    if not oitubeerror:
                        if not (await buttonhandler(bmsg, self_id, otbot_id, otbwait, otbsleeptimer, True,
                                                    "😌 please select, required format: 😬", "Video", "", "")):
                            await self._client.send_message(chatid, self.strings("watchererror", chatid))
                            await self._client.delete_messages(chatid, replymsgid_str)
                            await self._client.delete_messages(chatid, queuemsg)
                            if chat.admin_rights.delete_messages:
                                await self._client.delete_messages(chatid, msgid_str)
                        for otbdlvquality in otbdlvqualitys:
                            if await buttonhandler(bmsg, self_id, otbot_id, otbwait, otbsleeptimer, True,
                                                   "🙈 please select your required quality / format 🦾",
                                                   otbdlvquality, otbdlvformat, ""):
                                break
                        if not (await buttonhandler(bmsg, self_id, otbot_id, otbwait, otbsleeptimer, False,
                                                    "Upload Type: Video", "✅ Get TG File", "", "")):
                            await self._client.send_message(chatid, self.strings("watchererror", chatid))
                            await self._client.delete_messages(chatid, replymsgid_str)
                            await self._client.delete_messages(chatid, queuemsg)
                            if chat.admin_rights.delete_messages:
                                await self._client.delete_messages(chatid, msgid_str)
                        if not (await waitfortext(bmsg, self_id, otbot_id, otbqualwait, otbsleeptimer,
                                                  "📞 contacting YouTuBe to get 🕹 download details", "", "")):
                            await self._client.send_message(chatid, self.strings("watchererror", chatid))
                            await self._client.delete_messages(chatid, replymsgid_str)
                            await self._client.delete_messages(chatid, queuemsg)
                            if chat.admin_rights.delete_messages:
                                await self._client.delete_messages(chatid, msgid_str)
                        if not (await waitfortext(bmsg, self_id, otbot_id, otbdlwait, otbsleeptimer,
                                                  "🔜 uploading to Telegram", "", "")):
                            await self._client.send_message(chatid, self.strings("watchererror", chatid))
                            await self._client.delete_messages(chatid, replymsgid_str)
                            await self._client.delete_messages(chatid, queuemsg)
                            if chat.admin_rights.delete_messages:
                                await self._client.delete_messages(chatid, msgid_str)
                        if not (await waitfortext(bmsg, self_id, otbot_id, otbulwait, otbsleeptimer, "🍿", "👀", "|")):
                            await self._client.send_message(chatid, self.strings("watchererror", chatid))
                            await self._client.delete_messages(chatid, replymsgid_str)
                            await self._client.delete_messages(chatid, queuemsg)
                            if chat.admin_rights.delete_messages:
                                await self._client.delete_messages(chatid, msgid_str)
                        results = await self._client.get_messages(self_id, ids=bmsg.id)
                        await self._client.delete_messages(self_id, bmsg.id)
                        await self._client.send_message(chatid, results)
                        await self._client.delete_messages(chatid, queuemsg)
                        if chat.admin_rights.delete_messages:
                            await self._client.delete_messages(chatid, msgid_str)
                self.q.deQueue()
            else:
                await asyncio.sleep(1)

    async def watcher(self, message):
        vid_link = ""
        ytdlbot_id = self.ytdlbot_id
        otbot_id = self.otbot_id
        chatid = message.chat_id
        chatid_str = str(chatid)
        splitter = self.splitter
        if not hasattr(message, "id") or not isinstance(message, Message):
            return
        if message.id:
            msgid_str = str(message.id)
        chats = self._db.get(__name__, "watcher", [])
        archive = False
        if chatid_str not in chats or message.via_bot_id == ytdlbot_id or message.via_bot_id == otbot_id:
            return
        if message.sender_id == self.id:
            return
        for (ent, url) in message.get_entities_text():
            if isinstance(ent, MessageEntityUrl):
                match = re.search(self.ytregex, url)
                # if match:
                    # ytid = match.group(1)
                    # ytlink = True
                    # vid_link = ytid
                # else:
                    # ytlink = False
                    # vid_link = url
                ytlink = False
                vid_link = url
        if vid_link == "":
            return
        qcount = str(self.q.coQueue())
        replymsg = await self._client.send_message(chatid, self.strings("queued", message).format(vid_link, qcount))
        replymsgid_str = str(replymsg.id)
        return self.q.enQueue(vid_link + splitter + chatid_str + splitter + msgid_str + splitter + replymsgid_str
                              + splitter + str(ytlink) + splitter + str(archive))

    async def on_unload(self):
        self._task.cancel()

    async def ytdlcmd(self, message):
        """Available commands:
           .ytdl w <ID>
             - Adds the given id as watcher destination.
           .ytdl archive <ID>
             - Adds the given id as archive destination.
           .ytdl v <reply/link>
             - DL the video into the current chat.
           .ytdl av <reply/link>
             - DL the video into the archive.
           .ytdl timeout <time in seconds>
             - Sets the timeout to wait till error.
           .ytdl clearall
            - Clears the db of the module"""
        ytdlbot_id = self.ytdlbot_id
        chat = await message.get_chat()
        chatid = message.chat_id
        args = utils.get_args_raw(message)
        args = str(args).split()
        chats = self._db.get(__name__, "watcher", [])
        conf = self._db.get(__name__, "conf", {})
        if conf.get("timeout") == "" or conf.get("timeout") is None:
            timer = 600
        else:
            timer = int(conf.get("timeout"))
        archivechatid = conf.get("archive_id")
        if conf.get("archive_id") != "" and conf.get("archive_id") is not None:
            archivechatid = int(conf.get("archive_id"))
        elif args[0] == "av":
            return await utils.answer(message, self.strings("noarchive", message))
        if archivechatid != "":
            archivechatlinkid = str(archivechatid)[4:]
        if message.is_reply:
            replymsg = await message.get_reply_message()
            for (ent, url) in replymsg.get_entities_text():
                if isinstance(ent, MessageEntityUrl):
                    vid_link = url
                    args.append(vid_link)

        if args is None:
            return

        if args[0] == "clearall":
            self._db.set(__name__, "watcher", [])
            self._db.set(__name__, "conf", {})
            return await utils.answer(message, self.strings("turned_off", message))

        if args[0] == "av" or args[0] == "v" and args[1] is not None:
            if args[0] == "av":
                chatid = int(conf.get("archive_id"))
            else:
                chatid = chatid
            await utils.answer(message, self.strings("waiting", message))
            del args[0]
            args = (" ".join(str(x) for x in args))
            try:
                async with message.client.conversation(chat) as conv:
                    try:
                        response = conv.wait_event(events.NewMessage(incoming=True,
                                                                     from_users=ytdlbot_id), timeout=timer)
                        bmsg = await message.client.send_message(ytdlbot_id, args)
                        response = await response
                        await message.client.send_read_acknowledge(response.chat_id, response.message)
                    except YouBlockedUserError:
                        return await utils.answer(message, self.strings("unblock_bot", message))
                    except asyncio.TimeoutError:
                        msgs = await utils.answer(message, self.strings("timeout_error", message))
                        await asyncio.sleep(10)
                        return await message.client.delete_messages(chatid, msgs)
                    if "Too much links during five minutes" in response.text:
                        return await utils.answer(message, self.strings("max_videos", message))
                    elif ("Video unavailable" in response.text
                            or "is not a valid URL" in response.text
                            or "ERROR" in response.text):
                        return await utils.answer(message, response.message)
                    await message.client.send_message(chatid, response.message)
                    if chatid == archivechatid:
                        return await utils.answer(message, self.strings(
                            "chat_link", message).format(str(archivechatlinkid), str(bmsg.id)))
                    else:
                        return await message.client.delete_messages(chatid, message)
            except Exception as e:
                if "already has one open conversation" in str(e):
                    msgs = await utils.answer(message, self.strings("already_handling", message))
                    await asyncio.sleep(10)
                    return await message.client.delete_messages(chatid, msgs)
                else:
                    return logger.error(e)

        if args[0] == "w" and args[1] is not None:
            if not represents_int(args[1]):
                return await utils.answer(message, self.strings("no_int", message))
            else:
                if args[1] not in chats:
                    chats.append(args[1])
                else:
                    chats.remove(args[1])
                self._db.set(__name__, "watcher", chats)
            return await utils.answer(message, self.strings("settings", message).format(str(chats), str(conf)))

        if args[0] == "timeout" and args[1] is not None:
            if not represents_int(args[1]):
                return await utils.answer(message, self.strings("no_int", message))
            else:
                conf.update({"timeout": args[1]})
                self._db.set(__name__, "conf", conf)
        return await utils.answer(message, self.strings("settings", message).format(str(chats), str(conf)))

        if args[0] == "archive" and args[1] is not None:
            if not represents_int(args[1]):
                return await utils.answer(message, self.strings("no_int", message))
            else:
                conf.update({"archive_id": args[1]})
                self._db.set(__name__, "conf", conf)
        return await utils.answer(message, self.strings("settings", message).format(str(chats), str(conf)))

